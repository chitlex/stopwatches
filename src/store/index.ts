import Vue from 'vue'
import Vuex from 'vuex'
import {
  StartTicker, StopTicker
} from "@/store/types";

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    currentTicker: null,
    tickers: [
      { title: 'Работа', flex: 6, color: 'red', secondsPassed: 0, isActive: false, tickerId: 0 },
      { title: 'Перерыв', flex: 6, color: 'yellow', secondsPassed: 0, isActive: false, tickerId: 0 },
      { title: 'Встреча', flex: 6, color: 'green', secondsPassed: 0, isActive: false, tickerId: 0 },
      { title: 'Оффтоп', flex: 6, color: 'blue', secondsPassed: 0, isActive: false, tickerId: 0 },
    ]
  },
  mutations: {
  },
  actions: {
    [StartTicker]: (ctx: any, card: any) => {
      if (ctx.state.currentTicker) {
        clearInterval(ctx.state.currentTicker.tickerId)
        ctx.state.currentTicker.isActive = false
      }
      card.isActive = true
      card.tickerId = setInterval(() => {
        card.secondsPassed++
      }, 1000)

      Vue.set(ctx.state, 'currentTicker', card)
    },
    [StopTicker]: (ctx: any, card: any) => {
      card.isActive = false
      clearInterval(card.tickerId)
      Vue.set(ctx.state, 'currentTicker', null)
    },
  },
  modules: {
  }
})
