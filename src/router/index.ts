import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Tickers from '../views/Tickers.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Tickers',
    component: Tickers
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
