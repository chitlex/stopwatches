export function saveToFile(data: string, filename: string) {
    const b = new Blob([data], {type: 'application/json'});
    const a = document.createElement("a"),
        url = URL.createObjectURL(b);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(function () {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
    }, 0)
}